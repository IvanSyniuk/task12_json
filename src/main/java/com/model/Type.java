package com.model;

public class Type {
    private String theme;
    private String typeName;
    private String valueble;
    private boolean isSpended;

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setValueble(String valueble) {
        this.valueble = valueble;
    }

    public void setIsSpended(boolean isSpended) {
        isSpended = isSpended;
    }

    @Override
    public String toString() {
        return "Type{" +
                "theme='" + theme + '\'' +
                ", cardType='" + typeName + '\'' +
                ", valueble='" + valueble + '\'' +
                ", isSpended=" + isSpended +
                '}';
    }
}

package com.view;

import com.controller.MainControllerImpl;
import org.apache.logging.log4j.*;

import java.util.Scanner;

public class MainView {
    private static Logger logger = LogManager.getLogger(MainView.class);
    private MainControllerImpl mainController;
    private Scanner scanner;

    public MainView() {
        scanner = new Scanner(System.in);
        mainController = new MainControllerImpl();
    }
    private void showMenuInfo(){
        logger.info("1:Parse from json by Gson parser\n2:Parse from json by Json parser\n3:Exit");
    }

    public void showMenu() {
        boolean flag = true;
        do {
            showMenuInfo();
            switch (scanner.nextInt()) {
                case 1:
                    mainController.parseGson();
                    break;
                case 2:
                    mainController.parseJson();
                    break;
                case 3:
                    flag = false;
            }
        } while (flag);
    }
}
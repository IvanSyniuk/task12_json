package com.comparator;

import com.model.OldCard;

import java.util.Comparator;

public class  OldCardComparator implements Comparator<OldCard> {
    @Override
    public int compare(OldCard o1, OldCard o2) {
        return o1.getYear() - o2.getYear();
    }
}

package com.controller;

import com.parser.json.GSONParser;
import com.parser.json.JSONParser;
import com.parser.json.JSONValidator;

public class MainControllerImpl implements MainController {

    @Override
    public void parseGson() {
        GSONParser jsonParser = new GSONParser();
        JSONValidator jsonValidator = new JSONValidator();
        if(jsonValidator.isValid()){
            jsonParser.parse();
        }

    }

    @Override
    public void parseJson() {
        JSONParser jsonParser = new JSONParser();
        JSONValidator jsonValidator = new JSONValidator();
        if(jsonValidator.isValid()){
            jsonParser.parse();
        }
    }
}

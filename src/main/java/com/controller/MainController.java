package com.controller;

public interface MainController {

    void parseGson();

    void parseJson();

}

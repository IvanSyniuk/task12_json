package com.parser.json;

import com.comparator.OldCardComparator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.OldCard;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JSONParser {
    private static Logger logger = LogManager.getLogger(JSONParser.class);

    private OldCardComparator oldCardComparator = new OldCardComparator();

    public void parse() {
        ObjectMapper objectMapper = new ObjectMapper();
        List<OldCard> oldCardList;
        try {
            File jsonFile = new File("src\\main\\resources\\json\\OldCards.json");
            oldCardList = objectMapper.readValue(jsonFile, new TypeReference<List<OldCard>>() {
            });
            oldCardList.sort(oldCardComparator);
            for (OldCard o : oldCardList) {
                logger.info(o);
            }
        } catch (IOException e) {
            logger.error(e);
        }

    }
}

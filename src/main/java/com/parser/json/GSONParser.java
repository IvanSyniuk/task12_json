package com.parser.json;

import com.comparator.OldCardComparator;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.model.OldCard;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Type;

public class GSONParser {
    private static Logger logger = LogManager.getLogger(GSONParser.class);
    private OldCardComparator oldCardComparator = new OldCardComparator();

    public void parse() {
        List<OldCard> oldCards;
        Gson gson = new Gson();
        try {
            File jsonFile = new File("src\\main\\resources\\json\\OldCards.json");
            Type oldCardArrayType = new TypeToken<ArrayList<OldCard>>() {
            }.getType();
            oldCards = gson.fromJson(new FileReader(jsonFile), oldCardArrayType);
            oldCards.sort(oldCardComparator);
            for (OldCard o : oldCards) {
                logger.info(o);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
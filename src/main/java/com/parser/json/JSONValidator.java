package com.parser.json;

import java.io.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;

public class JSONValidator {
    static private Logger logger = LogManager.getLogger(JSONValidator.class);
    private File schemaPath = new File("src\\main\\resources\\json\\OldCardsSchema.json");
    private File oldCardJsonPath = new File("src\\main\\resources\\json\\OldCards.json");

    public boolean isValid(){
        try (InputStream inputStream = new FileInputStream(schemaPath)) {
            JSONObject rawSchema = new JSONObject(new JSONTokener(inputStream));
            Schema schema = SchemaLoader.load(rawSchema);
            schema.validate(new JSONArray(new JSONTokener(new FileInputStream(oldCardJsonPath))));
        } catch (IOException | ValidationException e) {
            logger.error(e.getMessage());
            return false;
        }
        return true;
    }
}
